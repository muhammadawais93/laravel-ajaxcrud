<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Simple CRUD with Ajax and Modal</title>

		<!-- Bootstrap -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>
	<body>
		<div class="container">
			@yield('content')
		</div>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

		<script type="text/javascript">
			$(document).on('click', '.edit-modal', function(){
				$('#footer-action-button').text('Update');
				$('#footer-action-button').addClass('glyphicon-check');
				$('#footer-action-button').addClass('glyphicon-trash');
				$('.actionBtn').addClass('btn-success');
				$('.actionBtn').removeClass('btn-danger');
				$('.actionBtn').addClass('Edit');
				$('.modal-title').text('Edit');
				$('.deleteContent').hide();
				$('form-horizontal').show();
				$('#fid').val($(this).data('id'));
				$('#t').val($(this).data('title'));
				$('#d').val($(this).data('description'));
				$('#myModal').modal('show');
			});

			$('.modal-footer').on('click', '.edit', function(){
				$.ajax({
					type: 'post',
					url: '/editItem',
					data: {
						'_token': $('input[name=_token]').val(),
						'id': $('#fid').val(),
						'title': $('#t').val(),
						'description': $('#d').val()
					},
					success: function(data) {
						$('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.title + "</td><td>" + data.description + "</td><td><button class='edit-modal btn btn-primary' data-id='" + data.id + "' data-title='" + data.title + "' data-description='" + data.description + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-title='" + data.title + "' data-description='" + data.description + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
					}
				});				
			});

			//add item 
			$('#add').click(function(){
				$.ajax({
					type: 'post',
					url: '/addItem',
					data: {
						'_token': $('input[name=_token]').val(),
						'title': $('input[name=title]').val(),
						'description': $('input[name=description]').val()
					},
					success: function(data){
						if(data.errors) {
							$('.error').removeClass('hidden');
							$('.error').text(data.errors.title);
							$('.error').text(data.errors.description);
						}else {
							$('.error').remove();
							$('#table').append("<tr class='item" + data.id + "'><td>" + data.id + "</td><td>" + data.title + "</td><td>" + data.description + "</td><td><button class='edit-modal btn btn-primary' data-id='" + data.id + "' data-title='" + data.title + "' data-description='" + data.description + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-title='" + data.title + "' data-description='" + data.description + "'><span class='glyphicon glyphicon-trash'></span> Delete</button></td></tr>");
						}
					}
				});
				$('#title').val('');
				$('#description').val('');
			});

			//del function
			$(document).on('click', '.delete-modal', function(){
				getId = $(this).data("id");
				$.ajax({
					type: 'post',
					url: '/deleteItem',
					data: {
						'_token': $('input[name=_token]').val(),
						'id': $(this).data("id")
					},
					success: function(data) {
						$('.item' + getId).remove();
					}
				});
			});
		</script>
	</body>
</html>